#REACH Custom CKEditor Build


##Changing this build

Read the [instructions](https://ckeditor.com/docs/ckeditor5/latest/builds/guides/development/custom-builds.html#updating-the-build)

##Update from original repo

`git remote add upstream https://github.com/ckeditor/ckeditor5-build-decoupled-document.git`
`git fetch upstream`
`git merge upstream/stable`

##Use [yarn](https://yarnpkg.com) to install dependencies

`yarn install`
