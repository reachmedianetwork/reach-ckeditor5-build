# Changelog

## unreleased

### Added in unreleased

### Changed in unreleased

### Deprecated in unreleased

### Fixed in unreleased

### Removed in unreleased

## 1.2.0

### Changed in 1.2.0

- Replaced alignment module with a custom one that always adds text alignment
css even if set to the default alignment

## 1.1.0

### Added in 1.1.0

- Forked repo to create a customized build of ckeditor5

- Added LineHeight module

- Transpile build to es5 with babel during build

### Removed in 1.1.0

- Removed unused modules Highlight, BlockQuote, List, Table, and TableToolbar